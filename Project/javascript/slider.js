$(document).ready(function(){
    $('.b-main-slider__content').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    prevArrow: '.b-main-slider__left-rifle',
    nextArrow: '.b-main-slider__right-rifle',
    responsive: [
    {
      breakpoint: 959,
      settings: {
        arrows: false
    }
    }
    ]
    });
});

$(document).ready(function(){
    $('.b-main-interesting-products__slider').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    prevArrow: '.b-main-interesting-products__all-products-left-rifle',
    nextArrow: '.b-main-interesting-products__all-products-right-rifle',
    responsive: [
    {
      breakpoint: 959,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false
        }
    },
    {
      breakpoint: 599,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false
        }
    }
    ]
    });
});

$(document).ready(function(){
  $('.b-main-brands__slider').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    prevArrow: '.b-main-brands__all-brands-content-left-rifle',
    nextArrow: '.b-main-brands__all-brands-content-right-rifle',
    responsive: [
    {
      breakpoint: 959,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false
    }
    }
    ]
    });
});

$(document).ready(function(){
  $('.b-main-recently-viewed__slider').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    prevArrow: '.b-main-recently-viewed__all-viewed-left-rifle',
    nextArrow: '.b-main-recently-viewed__all-viewed-right-rifle',
    responsive: [
    {
      breakpoint: 959,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false
    }
    },
    {
      breakpoint: 599,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false
    }
    }
    ]
    });
});